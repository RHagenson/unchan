# unchan

`unchan` is the unordered channel. Although typical desire when using a Go channel is to maintain order, there are some instances when that order is not the goal.
